/*
 * Copyright Andret Tools System (c) 2025. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.blockgenerator.entity;

import eu.andret.ats.blockgenerator.utils.RandomCollection;
import org.jetbrains.annotations.NotNull;

public record GeneratorPattern(@NotNull String name,
							   long delay,
							   @NotNull NamedItem generatorItem,
							   @NotNull NamedItem generatedItem,
							   @NotNull RandomCollection<NamedItem> dropItems) {
}
