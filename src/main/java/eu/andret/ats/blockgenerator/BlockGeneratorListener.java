/*
 * Copyright Andret Tools System (c) 2025. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.blockgenerator;

import eu.andret.ats.blockgenerator.entity.Generator;
import eu.andret.ats.blockgenerator.entity.GeneratorPattern;
import eu.andret.ats.blockgenerator.entity.NamedItem;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class BlockGeneratorListener implements Listener {
	private static final String GENERATOR = "generator";

	@NotNull
	private final BlockGeneratorPlugin plugin;

	public BlockGeneratorListener(@NotNull final BlockGeneratorPlugin plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void place(@NotNull final BlockPlaceEvent event) {
		final Block placed = event.getBlock();
		plugin.getPatternList()
				.stream()
				.filter(pattern -> Objects.equals(pattern.generatorItem().itemStack(), event.getItemInHand()))
				.findFirst()
				.map(pattern -> new Generator(pattern, placed))
				.ifPresent(generator -> {
					final GeneratorPattern pattern = generator.pattern();
					final Block relative = placed.getRelative(0, 1, 0);
					relative.setType(pattern.generatedItem().itemStack().getType());
					placed.setMetadata(GENERATOR, new FixedMetadataValue(plugin, pattern.name()));
				});
	}

	@EventHandler
	public void destroyGenerated(@NotNull final BlockBreakEvent event) {
		final Block brokenBlock = event.getBlock();
		final Block relative = brokenBlock.getRelative(0, -1, 0);
		final List<MetadataValue> relativeMetadata = relative.getMetadata(GENERATOR);
		if (relativeMetadata.isEmpty()) {
			return;
		}
		plugin.getPattern(relativeMetadata.get(0).asString()).ifPresent(pattern -> {
			event.setCancelled(true);
			brokenBlock.setType(Material.AIR);
			final Material material = pattern.generatedItem().itemStack().getType();
			final int id = plugin.getServer().getScheduler()
					.scheduleSyncDelayedTask(plugin, () -> brokenBlock.setType(material), pattern.delay());
			plugin.addScheduler(relative, id);
			if (Arrays.asList(GameMode.SURVIVAL, GameMode.ADVENTURE).contains(event.getPlayer().getGameMode())) {
				final NamedItem next = pattern.dropItems().next();
				if (next == null) {
					return;
				}
				final ItemStack dropItemStack = next.itemStack();
				Optional.of(brokenBlock)
						.map(Block::getLocation)
						.map(Location::getWorld)
						.ifPresent(world -> world.dropItemNaturally(brokenBlock.getLocation(), dropItemStack));
			}
		});
	}

	@EventHandler
	public void destroyGenerator(@NotNull final BlockBreakEvent event) {
		final Block brokenBlock = event.getBlock();
		final List<MetadataValue> metadata = brokenBlock.getMetadata(GENERATOR);
		if (metadata.isEmpty()) {
			return;
		}

		plugin.getPattern(metadata.get(0).asString()).ifPresent(pattern -> {
			event.setCancelled(true);
			if (Arrays.asList(GameMode.SURVIVAL, GameMode.ADVENTURE).contains(event.getPlayer().getGameMode())) {
				Optional.of(brokenBlock)
						.map(Block::getLocation)
						.map(Location::getWorld)
						.ifPresent(world -> world.dropItemNaturally(brokenBlock.getLocation(),
								pattern.generatorItem().itemStack()));
			}
			brokenBlock.setType(Material.AIR);
			brokenBlock.removeMetadata(GENERATOR, plugin);
			final Block relative = brokenBlock.getRelative(0, 1, 0);
			if (plugin.isSchedulerPresent(relative)) {
				plugin.cancelScheduler(relative);
			}
		});
	}
}
