/*
 * Copyright Andret Tools System (c) 2025. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.blockgenerator;

import eu.andret.arguments.AnnotatedCommand;
import eu.andret.arguments.CommandManager;
import eu.andret.arguments.api.entity.FallbackConstants;
import eu.andret.ats.blockgenerator.entity.GeneratorPattern;
import eu.andret.ats.blockgenerator.entity.NamedItem;
import eu.andret.ats.blockgenerator.utils.ConfigLoader;
import eu.andret.ats.blockgenerator.utils.RandomCollection;
import org.bstats.bukkit.Metrics;
import org.bukkit.block.Block;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import javax.annotation.processing.Generated;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Generated("Main class")
public class BlockGeneratorPlugin extends JavaPlugin {
	@NotNull
	private final List<GeneratorPattern> patternList = new ArrayList<>();
	@NotNull
	private final ConfigLoader configLoader = new ConfigLoader(this);
	@NotNull
	private final Map<Block, Integer> schedulers = new HashMap<>();

	@Override
	public void onEnable() {
		saveDefaultConfig();
		setUpListeners();
		patternList.addAll(configLoader.loadGeneratorPatterns());
		setUpCommand();
		new Metrics(this, 10330);
	}

	private void setUpCommand() {
		final AnnotatedCommand<BlockGeneratorPlugin> command = CommandManager
				.registerCommand(BlockGeneratorCommand.class, this);
		command.addArgumentCompleter("generatorName", () -> patternList.stream()
				.map(GeneratorPattern::generatorItem)
				.map(NamedItem::name)
				.collect(Collectors.toSet()));
		command.addArgumentMapper("generatorName", NamedItem.class, name -> patternList.stream()
						.map(GeneratorPattern::generatorItem)
						.filter(item -> item.name().equals(name))
						.findAny()
						.orElse(null),
				FallbackConstants.ON_NULL);
		command.addArgumentCompleter("generatedName", () -> patternList.stream()
				.map(GeneratorPattern::generatedItem)
				.map(NamedItem::name)
				.toList());
		command.addArgumentMapper("generatedName", NamedItem.class, name -> patternList.stream()
						.map(GeneratorPattern::generatedItem)
						.filter(item -> item.name().equals(name))
						.findAny()
						.orElse(null),
				FallbackConstants.ON_NULL);
		command.addArgumentCompleter("itemName", () -> patternList.stream()
				.map(GeneratorPattern::dropItems)
				.map(RandomCollection::getItems)
				.flatMap(Collection::stream)
				.map(NamedItem::name)
				.toList());
		command.addArgumentMapper("itemName", NamedItem.class, name -> patternList.stream()
						.map(GeneratorPattern::dropItems)
						.map(RandomCollection::getItems)
						.flatMap(Collection::stream)
						.filter(item -> item.name().equals(name))
						.findAny()
						.orElse(null),
				FallbackConstants.ON_NULL);
	}

	private void setUpListeners() {
		getServer().getPluginManager().registerEvents(new BlockGeneratorListener(this), this);
	}

	@NotNull
	public Optional<GeneratorPattern> getPattern(@NotNull final String name) {
		return patternList.stream()
				.filter(generatorPattern -> generatorPattern.name().equals(name))
				.findFirst();
	}

	@NotNull
	public List<GeneratorPattern> getPatternList() {
		return patternList;
	}

	public void addScheduler(@NotNull final Block block, final int id) {
		schedulers.put(block, id);
	}

	public boolean isSchedulerPresent(@NotNull final Block block) {
		return schedulers.containsKey(block);
	}

	public void cancelScheduler(@NotNull final Block block) {
		getServer().getScheduler().cancelTask(schedulers.get(block));
		schedulers.remove(block);
	}
}
